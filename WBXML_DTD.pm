package WBXML_DTD;

use strict;
use warnings;

# SyncML Tokens (Representation Protocol section 8)
my $_syncml_tokens = {
    Add             => 0x05,
    Alert           => 0x06,
    Archive         => 0x07,
    Atomic          => 0x08,
    Chal            => 0x09,
    Cmd             => 0x0a,
    CmdID           => 0x0b,
    CmdRef          => 0x0c,
    Copy            => 0x0d,
    Cred            => 0x0e,
    Data            => 0x0f,
    Delete          => 0x10,
    Exec            => 0x11,
    Final           => 0x12,
    Get             => 0x13,
    Item            => 0x14,
    Lang            => 0x15,
    LocName         => 0x16,
    LocURI          => 0x17,
    Map             => 0x18,
    MapItem         => 0x19,
    Meta            => 0x1a,
    MsgID           => 0x1b,
    MsgRef          => 0x1c,
    NoResp          => 0x1d,
    NoResults       => 0x1e,
    Put             => 0x1f,
    Replace         => 0x20,
    RespURI         => 0x21,
    Results         => 0x22,
    Search          => 0x23,
    Sequence        => 0x24,
    SessionID       => 0x25,
    SftDel          => 0x26,
    Source          => 0x27,
    SourceRef       => 0x28,
    Status          => 0x29,
    Sync            => 0x2a,
    SyncBody        => 0x2b,
    SyncHdr         => 0x2c,
    SyncML          => 0x2d,
    Target          => 0x2e,
    TargetRef       => 0x2f,
    _reserved       => 0x30,
    VerDTD          => 0x31,
    VerProto        => 0x32,
    NumberOfChanges => 0x33,
    MoreData        => 0x34,
    Field           => 0x35,
    Filter          => 0x36,
    Record          => 0x37,
    FilterType      => 0x38,
    SourceParent    => 0x39,
    TargetParent    => 0x3a,
    Move            => 0x3b,
    Correlator      => 0x3c,
};

# MetaInfo tokens (MetaInfo section 7.2)
my $_metainfo_tokens = {
    Anchor      => 0x05,
    EMI         => 0x06,
    Format      => 0x07,
    FreeID      => 0x08,
    FreeMem     => 0x09,
    Last        => 0x0a,
    Mark        => 0x0b,
    MaxMsgSize  => 0x0c,
    Mem         => 0x0d,
    MetInf      => 0x0e,
    Next        => 0x0f,
    NextNonce   => 0x10,
    SharedMem   => 0x11,
    Size        => 0x12,
    Type        => 0x13,
    Version     => 0x14,
    MaxObjSize  => 0x15,
    FieldLevel  => 0x16,
};

#TODO DevInfo tokens (Device Information section 7)
my $_devinfo_tokens = {};

our $SyncML_DTD = {
    publicid => '-//SYNCML//DTD SyncML 1.2//EN',
    # WBXML tokens for each codepage
    tokens => [
        {tok => $_syncml_tokens, rtok => {reverse %$_syncml_tokens}, namespace=> undef},
        {tok => $_metainfo_tokens, rtok => {reverse %$_metainfo_tokens}, namespace=> 'MetaInfo'}
    ]
};

our $MetaInfo_DTD = {
    publicid => '-//SYNCML//DTD MetInf 1.2//EN',
    # WBXML tokens for each codepage
    tokens => [
        {tok => $_metainfo_tokens, rtok => {reverse %$_metainfo_tokens}, namespace=> 'MetaInfo'}
    ]
};

our $DevInfo_DTD = {
    publicid => '-//SYNCML//DTD DevInf 1.2//EN',
    # WBXML tokens for each codepage
    tokens => [
        {tok => $_devinfo_tokens, rtok => {reverse %$_devinfo_tokens}, namespace=> undef},
    ]
};

1;

=encoding utf8

=head1 DTD for the WBXML documents

=head1 Author Miklós Máté E<lt>mtmkls@gmail.comE<gt>

=head1 License MIT

=cut
