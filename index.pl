#!/usr/bin/perl
# simple SyncML server for receiving data from a phone
use strict;
use warnings;

use lib '.';

use BinaryDocument;
use WBXML_Parser;
use WBXML_DTD;

use Data::Dumper;

my $work_dir = '/home/miki/syncml'; # www-data must have write permission on this dir

# find the first child of $start with name $name, return the child of that
sub get_value {
    my $xml = shift;
    my $name = shift;
    my $start = shift;

    my $ret;
    $xml->find_elem_by_name($name, sub {my $x = shift; $ret = $x->get_first_child unless $ret}, $start);
    return $ret
}

undef $/; # slurp the stdin

if ($ENV{REQUEST_METHOD} eq 'POST') {
    my $input = <STDIN>;

    open (my $DBG, ">>", "$work_dir/debuglog.txt") or die "Cannot open: $!\n";
    print $DBG "got POST from $ENV{REMOTE_ADDR} type '$ENV{CONTENT_TYPE}' at ", (scalar localtime), "\n";

    # first: save the request to a file for offline analysis
    my $ofile = "$work_dir/$ENV{REMOTE_ADDR} " . (scalar localtime);
    # if we receive multiple messages within a second, add a serial number
    if (-r "$ofile in.txt") {
        my $serial = 1;
        $serial++ while -r "$ofile $serial in.txt";
        $ofile = "$ofile $serial"
    }
    open (my $F, ">", "$ofile in.txt") or die "Cannot open: $!\n";
    binmode $F;
    print $F $input;
    close $F;
    print $DBG "saved the post to '$ofile.txt'\n";

    if ($ENV{CONTENT_TYPE} eq 'application/vnd.syncml+wbxml') {

        my $reqd = new BinaryDocument;
        $reqd->set_string($input);

        my $parser = new WBXML_Parser;
        $parser->set_dtd($WBXML_DTD::SyncML_DTD);
        my $reqxml = $parser->parse($reqd);

        # process the request header
        my $hdr; $reqxml->find_elem_by_name('SyncHdr', sub {$hdr = shift});
        my $sessionID  = get_value($reqxml, 'SessionID', $hdr);
        my $msgID      = get_value($reqxml, 'MsgID', $hdr);
        my $target     = get_value($reqxml, 'Target', $hdr)->get_first_child; # it's embedded in a LocURI
        my $source     = get_value($reqxml, 'Source', $hdr)->get_first_child; # it's embedded in a LocURI
        my $maxMsgSize = get_value($reqxml, 'MetaInfo:MaxMsgSize', $hdr);
        print $DBG "sessionID $sessionID msgID $msgID target $target source $source maxMsgSize $maxMsgSize\n";

        # create response, fill the header fields
        my $resp = new WBXML;
        $resp->create_syncml_skeleton;
        my $resphdr; $resp->find_elem_by_name('SyncHdr', sub {$resphdr = shift});
        my $respbody; $resp->find_elem_by_name('SyncBody', sub {$respbody = shift});
        $resphdr->add_child(new WBXML_Elem('SessionID'))->add_child($sessionID); # stay in the session
        $resphdr->add_child(new WBXML_Elem('MsgID'))->add_child($msgID); # follow the client's numbering
        $resphdr->add_child(new WBXML_Elem('Target'))->add_child(new WBXML_Elem('LocURI'))->add_child($source);
        $resphdr->add_child(new WBXML_Elem('Source'))->add_child(new WBXML_Elem('LocURI'))->add_child($target);
        #$resphdr->add_child(new WBXML_Elem('Meta'))->add_child(new WBXML_Elem('MetaInfo:MaxMsgSize'))->add_child($maxMsgSize);

        my $body; $reqxml->find_elem_by_name('SyncBody', sub {$body = shift});

        my $respCmdID = 1; # count the commands in the response

        # we need to separately accept the header of the request of all messages
        if (1) {
            my $clcred; $reqxml->find_elem_by_name('Cred', sub {$clcred = shift}, $hdr); # client auth request
            my $hdrstatus = $respbody->add_child(new WBXML_Elem('Status'));
            $hdrstatus->add_child(new WBXML_Elem('CmdID'))->add_child($respCmdID++);
            $hdrstatus->add_child(new WBXML_Elem('MsgRef'))->add_child($msgID);
            $hdrstatus->add_child(new WBXML_Elem('CmdRef'))->add_child(0);
            $hdrstatus->add_child(new WBXML_Elem('Cmd'))->add_child('SyncHdr');
            $hdrstatus->add_child(new WBXML_Elem('TargetRef'))->add_child($target);
            $hdrstatus->add_child(new WBXML_Elem('SourceRef'))->add_child($source);
            $hdrstatus->add_child(new WBXML_Elem('Data'))->add_child($clcred ? 212 : 200); # auth ? auth success : ok
        }

        for my $cmd (@{$body->{children}}) {
            my $cmdname = $cmd->{name};
            print $DBG "command $cmdname\n";

            if ($cmdname eq 'Put') {
                my $put_cmdid = get_value($reqxml, 'CmdID', $cmd);
                print $DBG "Put command ($put_cmdid), received capabilities ignored, status: 200 OK\n";
                #TODO we need a DevInf DTD to decode the device capabilities

                # reply with 'Status', code: 200 OK
                my $status = $respbody->add_child(new WBXML_Elem('Status'));
                $status->add_child(new WBXML_Elem('CmdID'))->add_child($respCmdID++);
                $status->add_child(new WBXML_Elem('MsgRef'))->add_child($msgID);
                $status->add_child(new WBXML_Elem('CmdRef'))->add_child($put_cmdid);
                $status->add_child(new WBXML_Elem('Cmd'))->add_child('Put');
                $status->add_child(new WBXML_Elem('SourceRef'))->add_child('./devinf12');
                $status->add_child(new WBXML_Elem('Data'))->add_child(200); # HTTP OK

            } elsif ($cmdname eq 'Get') {
                print $DBG "Get command ignored, let's hope this is not important\n";
                #TODO must reply with Results
                #TODO oh shit, we have to invent our fake capabilities
                #TODO save the phone's capability XML from the Put command and send it back?
                #TODO fortunately the phone is not interested in our capabilities

            } elsif ($cmdname eq 'Alert') {
                my $alert_cmdid  = get_value($reqxml, 'CmdID', $cmd);
                my $alert_data   = get_value($reqxml, 'Data', $cmd); # this is the sync type (201 means slow)
                my $alert_target = get_value($reqxml, 'Target', $cmd)->get_first_child;
                my $alert_source = get_value($reqxml, 'Source', $cmd)->get_first_child;
                my $alert_last   = get_value($reqxml, 'MetaInfo:Last', $cmd);
                my $alert_next   = get_value($reqxml, 'MetaInfo:Next', $cmd);
                print $DBG "Alert command ($alert_cmdid) data $alert_data target '$alert_target' source '$alert_source' anchors: $alert_last -- $alert_next\n";

                # reply with 'Status', code: 200 OK
                my $status = $respbody->add_child(new WBXML_Elem('Status'));
                $status->add_child(new WBXML_Elem('CmdID'))->add_child($respCmdID++);
                $status->add_child(new WBXML_Elem('MsgRef'))->add_child($msgID);
                $status->add_child(new WBXML_Elem('CmdRef'))->add_child($alert_cmdid);
                $status->add_child(new WBXML_Elem('Cmd'))->add_child('Alert');
                $status->add_child(new WBXML_Elem('TargetRef'))->add_child($alert_target);
                $status->add_child(new WBXML_Elem('SourceRef'))->add_child($alert_source);
                $status->add_child(new WBXML_Elem('Data'))->add_child(200); # HTTP OK
                my $respitem = $status->add_child(new WBXML_Elem('Item'));
                my $respdata = $respitem->add_child(new WBXML_Elem('Data'));
                my $respanchor = $respdata->add_child(new WBXML_Elem('MetaInfo:Anchor'));
                $respanchor->add_child(new WBXML_Elem('MetaInfo:Next'))->add_child($alert_next);

                # also reply with an Alert, same operation code as in the alert of the phone
                my $ralert = $respbody->add_child(new WBXML_Elem('Alert'));
                $ralert->add_child(new WBXML_Elem('CmdID'))->add_child($respCmdID++);
                $ralert->add_child(new WBXML_Elem('Data'))->add_child($alert_data); # same code
                my $ralitem = $ralert->add_child(new WBXML_Elem('Item'));
                $ralitem->add_child(new WBXML_Elem('Target'))->add_child(new WBXML_Elem('LocURI'))->add_child($alert_source);
                $ralitem->add_child(new WBXML_Elem('Source'))->add_child(new WBXML_Elem('LocURI'))->add_child($alert_target);
                my $ralmeta = $ralitem->add_child(new WBXML_Elem('Meta'));
                my $ralanchor = $ralmeta->add_child(new WBXML_Elem('MetaInfo:Anchor'));
                $ralanchor->add_child(new WBXML_Elem('MetaInfo:Last'))->add_child(0); # start at the beginning
                $ralanchor->add_child(new WBXML_Elem('MetaInfo:Next'))->add_child($alert_next);
                $ralmeta->add_child(new WBXML_Elem('MetaInfo:MaxObjSize'))->add_child(65535);

            } elsif ($cmdname eq 'Sync') {
                # this is a transaction in the synchronization session
                my $sync_cmdid     = get_value($reqxml, 'CmdID', $cmd);
                my $sync_target    = get_value($reqxml, 'Target', $cmd)->get_first_child;
                my $sync_source    = get_value($reqxml, 'Source', $cmd)->get_first_child;
                my $sync_freemem   = get_value($reqxml, 'MetaInfo:FreeMem', $cmd); #TODO the phone doesn't send these
                my $sync_freeid    = get_value($reqxml, 'MetaInfo:FreeId', $cmd);
                my $sync_numchange = get_value($reqxml, 'NumberOfChanges', $cmd);
                #print $DBG "Sync command ($sync_cmdid) target '$sync_target' source '$sync_source' free mem $sync_freemem free id $sync_freeid number of changes $sync_numchange\n";
                print $DBG "Sync command ($sync_cmdid) target '$sync_target' source '$sync_source' number of changes $sync_numchange\n";

                # reply with 'Status', code: 200 OK
                my $status = $respbody->add_child(new WBXML_Elem('Status'));
                $status->add_child(new WBXML_Elem('CmdID'))->add_child($respCmdID++);
                $status->add_child(new WBXML_Elem('MsgRef'))->add_child($msgID);
                $status->add_child(new WBXML_Elem('CmdRef'))->add_child($sync_cmdid);
                $status->add_child(new WBXML_Elem('Cmd'))->add_child('Sync');
                $status->add_child(new WBXML_Elem('TargetRef'))->add_child($sync_target);
                $status->add_child(new WBXML_Elem('SourceRef'))->add_child($sync_source);
                $status->add_child(new WBXML_Elem('Data'))->add_child(200); # HTTP OK

                # the modification commands are embedded in the Sync command
                # the phone seems to only use Replace
                # other valid commands: Add, Copy, Delete, Atomic, Sequence
                # we need to send Status for each command
                $reqxml->find_elem_by_name('Replace', sub {
                        my $rcmd = shift;

                        my $replace_cmdid  = get_value($reqxml, 'CmdID', $rcmd);
                        my $replace_type   = get_value($reqxml, 'MetaInfo:Type', $rcmd);
                        my $replace_source = get_value($reqxml, 'Source', $rcmd)->get_first_child;
                        my $replace_data   = get_value($reqxml, 'Data', $rcmd); # this is our McGuffin
                        my $replace_moredata; $reqxml->find_elem_by_name('MoreData', sub {$replace_moredata = shift}, $rcmd);
                        print $DBG "Replace command ($replace_cmdid) type '$replace_type' source '$replace_source' ",
                            ($replace_moredata?'partial':''), "\n";
                        #print $DBG " data is ", ref $replace_data, "\n";

                        # save the vcard, handle continuations
                        my $vcardname = "$work_dir/$ENV{REMOTE_ADDR} $replace_source.vcard";
                        open (my $VCF, ">>", $vcardname) or die "Cannot open: $!\n";
                        binmode $VCF;
                        print $VCF $replace_data->{data}; # assume that it's OPAQUE
                        close $VCF;

                        my $rstatus = $respbody->add_child(new WBXML_Elem('Status'));
                        $rstatus->add_child(new WBXML_Elem('CmdID'))->add_child($respCmdID++);
                        $rstatus->add_child(new WBXML_Elem('MsgRef'))->add_child($msgID);
                        $rstatus->add_child(new WBXML_Elem('CmdRef'))->add_child($replace_cmdid);
                        $rstatus->add_child(new WBXML_Elem('Cmd'))->add_child('Replace');
                        $rstatus->add_child(new WBXML_Elem('SourceRef'))->add_child($replace_source);
                        if ($replace_moredata) {
                            # we only got part of this vcard, we must ask for the rest of it
                            # reply with 'Status', code 213
                            $rstatus->add_child(new WBXML_Elem('Data'))->add_child(213); # HTTP chunk accepted&buffered

                            # also reply with 'Alert', code 222 (NEXT_MESSAGE)
                            # this is optional, so I skip it :)
                            #TODO anchors?
                            if (0) {
                            my $ralert = $respbody->add_child(new WBXML_Elem('Alert'));
                            $ralert->add_child(new WBXML_Elem('CmdID'))->add_child($respCmdID++);
                            $ralert->add_child(new WBXML_Elem('Data'))->add_child(222); # send more data
                            my $ralitem = $ralert->add_child(new WBXML_Elem('Item'));
                            $ralitem->add_child(new WBXML_Elem('Target'))->add_child(new WBXML_Elem('LocURI'))->add_child($sync_source);
                            $ralitem->add_child(new WBXML_Elem('Source'))->add_child(new WBXML_Elem('LocURI'))->add_child($sync_target);
                            my $ralmeta = $ralitem->add_child(new WBXML_Elem('Meta'));
                            my $ralanchor = $ralmeta->add_child(new WBXML_Elem('MetaInfo:Anchor'));
                            $ralanchor->add_child(new WBXML_Elem('MetaInfo:Last'))->add_child($replace_source);
                            $ralanchor->add_child(new WBXML_Elem('MetaInfo:Next'))->add_child($replace_source);
                            }

                        } else {
                            # reply with 'Status', code: 200 OK
                            $rstatus->add_child(new WBXML_Elem('Data'))->add_child(200); # HTTP OK
                        }

                    }, $cmd);

            } elsif ($cmdname eq 'Status') {
                my $status_cmdid = get_value($reqxml, 'CmdID', $cmd);
                my $status_data  = get_value($reqxml, 'Data', $cmd); # this is the HTTP result code
                print $DBG "Status command ($status_cmdid) code $status_data\n";

            } elsif ($cmdname eq 'Final') {
                print $DBG "Final\n";

            } else {
                print $DBG "unhandled command '$cmdname'\n"
            }
        }

        # we must always add a Final tag at the end
        $respbody->add_child(new WBXML_Elem('Final'));

        #$resp->printxml($DBG);

        my $respdoc = $parser->encode($resp);

        # save the encoded response for offline analysis
        open (my $RF, ">", "$ofile out.txt") or die "Cannot open: $!\n";
        binmode $RF;
        print $RF $respdoc->get_string;
        close $RF;

        # send the encoded response
        print "Content-Type: application/vnd.syncml+wbxml\nCache-Control: no-cache\nStatus: 200\n\n";
        print $respdoc->get_string

    } else {
        # this response is just for testing, it's invalid for the phone!
        print "Content-Type: text/html\nStatus: 200\n\n";
        print "<html><head></head><body><h1>Thanks for the post!</h1><pre>";
        print "You sent '$ENV{CONTENT_TYPE}' length $ENV{CONTENT_LENGTH} : '$input'\n";
        print "ofile '$ofile'\n";
        print "</pre></body></html>\n"
    }


} else {
    # this is only for testing with Firefox, SyncML only uses POST
    print "Content-Type: text/html\nStatus: 200\n\n";
    print "<html><head></head><body><h1>Hello Perl!</h1>";
    print "<p>", (scalar localtime), "</p>\n";
    print "<p>Your headers:</p><pre>", Dumper(\%ENV), "</pre>\n";;
    print "</body></html>\n"
}

=encoding utf8

=head1 SyncML server

=head1 Author Miklós Máté E<lt>mtmkls@gmail.comE<gt>

=head1 License MIT

=cut
