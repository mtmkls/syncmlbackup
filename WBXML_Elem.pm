package WBXML_Elem;

use strict;
use warnings;

sub new {
    my $class = shift;
    my $name = shift;
    my $self = bless {}, $class;
    $self->{name} = $name if $name;
    return $self
}

# child can be another WBXML_Elem or any other valid type
sub add_child {
    my $self = shift;
    my $child = shift;

    push @{$self->{children}}, $child;
    return $child
}

sub get_first_child {
    my $self = shift;
    return $self->{children}[0]
}

1;

=encoding utf8

=head1 One element of the WBXML tree

=head1 Author Miklós Máté E<lt>mtmkls@gmail.comE<gt>

=head1 License MIT

=cut
