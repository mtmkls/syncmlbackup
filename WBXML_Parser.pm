package WBXML_Parser;

use strict;
use warnings;
no warnings qw(experimental::smartmatch);

use WBXML;

# WBXML Global Tokens (WAP Binary XMP Spec. section 7.1)
my %wbxml_tokens = (
    WB_SWITCH_PAGE  => 0x00,
    WB_END          => 0x01,
    WB_ENTITIY      => 0x02,
    WB_STR_I        => 0x03,
    WB_LITERAL      => 0x04,
    WB_EXT_I_0      => 0x40,
    WB_EXT_I_1      => 0x41,
    WB_EXT_I_2      => 0x42,
    WB_PI           => 0x43,
    WB_LITERAL_C    => 0x44,
    WB_EXT_T_0      => 0x80,
    WB_EXT_T_1      => 0x81,
    WB_EXT_T_2      => 0x82,
    WB_STR_T        => 0x83,
    WB_LITERAL_A    => 0x84,
    WB_EXT_0        => 0xc0,
    WB_EXT_1        => 0xc1,
    WB_EXT_2        => 0xc2,
    WB_OPAQUE       => 0xc3,
    WB_LITERAL_AC   => 0xc4,
);

my %tokens_wbxml = reverse %wbxml_tokens;


sub new {
    my $class = shift;
    my $self = bless {page=>0}, $class;
    return $self
};

sub set_dtd {
    my $self = shift;
    my $dtd = shift;

    $self->{dtd} = $dtd;
}

# multi-byte integer format (section 5.1)
sub read_mbint {
    my $self = shift;
    my $doc = shift;

    my @seq;
    my $c;
    do {
        $c = $doc->getc;
        unshift @seq, $c
    } while ($c & 0x80);

    my $ret = 0;
    for my $i (0..$#seq) {
        $ret += ($seq[$i] & 0x7f) << (7*$i)
    }
    #print "read_mbint $ret\n";
    return $ret
}

# return the given number in multi-byte integer format (section 5.1) as a list of characters
sub write_mbint {
    my $self = shift;
    my $num = shift;

    return [$num] if $num < 0x7f;

    my @parts;
    while ($num) {
        unshift @parts, $num &0x7f;
        $num = $num >> 7;
    }
    for my $i (0..$#parts-1) {
        $parts[$i] = 0x80 | $parts[$i];
    }
    #print "write_mbint ", join(',', @parts), "\n";
    return [@parts];
}

sub parse_builtin {
    my $self = shift;
    my $doc = shift;
    my $type = shift;

    if ($type eq 'WB_STR_I') {
        #print "parsing string: ";
        my @str;
        my $chr;
        push @str, $chr while $chr = $doc->getc;
        my $str = pack "C*", @str;
        #print "'$str'\n";
        return $str
    } elsif ($type eq 'WB_END') {
        #print "end of something\n";
        return
    } elsif ($type eq 'WB_SWITCH_PAGE') {
        my $newpage = $doc->getc;
        #print "switching to page $newpage\n";
        $self->{page} = $newpage;
        return []
    } elsif ($type eq 'WB_OPAQUE') {
        my $len = $self->read_mbint($doc);
        #print "parsing opaque data, length $len\n";
        my @data;
        push @data, $doc->getc for (1..$len);
        return WBXML::create_opaque(@data)
    }
    #TODO all the other types
    die "don't yet know how to parse '$type'\n"
}

sub parse_elem {
    my $self = shift;
    my $doc = shift;

    my $elemname = $doc->getc;
    my $elem_tag = $elemname & 0x3f;
    my $has_child = $elemname & 0x40;
    my $has_attr = $elemname & 0x80;

    my $name = $self->{dtd}{tokens}[$self->{page}]{rtok}{$elem_tag};
    if (defined $name) {
        my $ns = $self->{dtd}{tokens}[$self->{page}]{namespace};
        $name = "$ns:$name" if defined $ns;
        #print "elem '$name'\n";
        my $xelem = new WBXML_Elem($name);
        #$xelem->{name} = $name;
        if ($has_attr) {
            print "has attributes\n";
            #TODO each attribute is an 1byte codename, then its value (usually WB_STR_I)
            #TODO end of attribute list is WB_END (section 5.8.3)
            die "I don't think SyncML uses attributes...\n"
            #TODO push them into $xelem->{attrs}[] array
        }
        if ($has_child) {
            #printf "has children\n";
            my $child;
            while (1) {
                my $child = $self->parse_elem($doc);
                last unless defined $child; # we get undef on WB_END
                next if ref $child eq 'ARRAY'; # not a real element, e.g. switching codepages
                $xelem->add_child($child)
            }
        }
        return $xelem
    } else {
        # built-in types use the full 8 bit, because they never have attributes or children
        $name = $tokens_wbxml{$elemname};
        if (defined $name) {
            #print "elem is a built-in type of '$name'\n";
            return $self->parse_builtin($doc, $name)
        } else {
            die "tag $elem_tag is not known in code page $self->{page}\n"
        }
    }
}

# no DTD is required to parse the header
sub parse_header {
    my $self = shift;
    my $doc = shift;

    my $ret = new WBXML;

    # get header
    my $ver = $doc->getc;
    my $vermaj = 1 + ($ver>>4);
    my $vermin = $ver & 0xf;
    $ret->{version} = "$vermaj.$vermin";

    my $pubid = $self->read_mbint($doc);
    if ($pubid == 0) {
        # this means the next mbint is the id, which is an index into the string table
        $pubid = $self->read_mbint($doc);
    } else {
        # nonzero numbers refer to predefined strings (none of them are relevant to us)
    }
    $ret->{pubid} = $pubid;

    my $chrset = $doc->getc;
    #TODO decode it according to https://www.iana.org/assignments/character-sets/character-sets.xhtml
    # the phone always uses 0x6a (UTF-8) so I skip that
    $ret->{charset} = $chrset;

    my $strtbllen = $self->read_mbint($doc);
    my @strtbl;
    push @strtbl, $doc->getc for (1..$strtbllen);
    $ret->{stringtable} = pack "C*", @strtbl;

    return $ret
}

sub parse {
    my $self = shift;
    my $doc = shift;

    die "parser: can't get char from the supplied document\n" unless $doc->can("getc");
    die "parser: no DTD supplied\n" unless defined $self->{dtd};

    my $ret = $self->parse_header($doc);

    my $retpubstr = $ret->get_pubid_string;
    die "parser: document's public ID '$retpubstr' does not match the DTD\n"
        unless $retpubstr eq $self->{dtd}{publicid};

    # decode the XML document itself
    $self->{page} = 0;
    $ret->{root} = $self->parse_elem($doc);

    return $ret
}

sub encode {
    my $self = shift;
    my $xml = shift;

    my $ret = new BinaryDocument;
    # take a WBXML and return a BinaryDocument

    # write header
    my ($vermaj, $vermin) = $xml->{version} =~ /(\d)\.(\d)/;
    #print "version $vermaj $vermin\n";
    my $ver = ($vermaj-1)<<4 | $vermin;
    $ret->putc($ver);

    if ($xml->{pubid}) {
        my $pid = $self->write_mbint($xml->{pubid});
        $ret->putc($_) for @$pid
    } else {
        $ret->putc(0);
        $ret->putc(0);
    }

    # I didn't decode this, so it should be 0x6a
    $ret->putc($xml->{charset});

    my $strtbllen = $self->write_mbint(length $xml->{stringtable});
    $ret->putc($_) for @$strtbllen;
    $ret->putc($_) for unpack "C*", $xml->{stringtable};

    # process the xml tree
    $self->{page} = 0;
    $xml->traverse($xml->{root}, {
            elemname => sub {
                my ($name, $hasattr, $haschild) = @_;

                my (undef, $namespace, $simplename) = $name =~ /((\w+):)?(\w+)/;
                #print "elem name '$name' ", defined $namespace ? "namespace '$namespace'" : "no namespace", " simplename '$simplename'\n";

                # check if we are on the correct codepage, switch if not
                unless ($namespace ~~ $self->{dtd}{tokens}[$self->{page}]{namespace}) {
                    #print "need to change codepage\n";
                    for my $p (0..$#{$self->{dtd}{tokens}}) {
                        if ($namespace ~~ $self->{dtd}{tokens}[$p]{namespace}) {
                            $self->{page} = $p;
                            last
                        }
                    }
                    die "no codepage for namespace '$namespace'\n"
                        unless $namespace ~~ $self->{dtd}{tokens}[$self->{page}]{namespace};
                    #print "new codepage is $self->{page}\n";
                    $ret->putc(0);
                    $ret->putc($self->{page});
                }

                my $tag = $self->{dtd}{tokens}[$self->{page}]{tok}{$simplename};
                die "invalid element name '$simplename' for code page $self->{page}\n" unless defined $tag;
                #print "element name '$simplename' code page $self->{page} tag $tag\n";
                $tag |= 0x40 if $haschild;
                $tag |= 0x80 if $hasattr;
                $ret->putc($tag)
            },
            attr => sub {
                my ($name, $value) = @_;
                die "I don't think SyncML uses attributes...\n"
            },
            elem_done => sub {
                my ($name, $hasattr, $haschild) = @_;
                #print "elem done $name\n";
                $ret->putc($wbxml_tokens{WB_END}) if $haschild
            },
            string => sub {
                my $str = shift;
                $ret->putc($wbxml_tokens{WB_STR_I});
                $ret->putc($_) for unpack "C*", $str;
                $ret->putc(0);
            },
            opaque => sub {
                my $opaque = shift;
                $ret->putc($wbxml_tokens{WB_OPAQUE});
                my $oplen = $self->write_mbint(length $opaque);
                $ret->putc($_) for @$oplen;
                $ret->putc($_) for unpack "C*", $opaque
            },
        });

    return $ret
}

1;

=encoding utf8

=head1 Parser for WBXML

=head1 Author Miklós Máté E<lt>mtmkls@gmail.comE<gt>

=head1 License MIT

=cut
