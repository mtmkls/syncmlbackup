package BinaryDocument;

use strict;
use warnings;

sub new {
    my $class = shift;
    my $self = bless {pos=>0}, $class;
    return $self
};

sub set_string {
    my $self = shift;
    my $str = shift;
    $self->{list} = [unpack "C*", $str];
    $self->{pos} = 0;
}

sub set_list {
    my $self = shift;
    my $lst = shift;
    $self->{list} = $lst;
    $self->{pos} = 0;
}

sub getc {
    my $self = shift;
    #TODO check for overflow
    return $self->{list}[$self->{pos}++]
}

#sub ungetc {
#    my $self = shift;
#    $self->{pos}--
#}

sub putc {
    my $self = shift;
    my $c = shift;
    #print "putc $c\n";
    push @{$self->{list}}, $c
}

sub get_string {
    my $self = shift;
    return pack "C*", @{$self->{list}}
}

sub reset_pos {
    my $self = shift;
    $self->{pos} = 0;
}

1;

=encoding utf8

=head1 WBXML encoded message stored as a list of bytes

=head1 Author Miklós Máté E<lt>mtmkls@gmail.comE<gt>

=head1 License MIT

=cut
