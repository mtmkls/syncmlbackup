#!/usr/bin/perl
# test sandbox for the SyncML WBXML parser
use strict;
use warnings;

use lib '.';

use BinaryDocument;
use WBXML_Parser;
use WBXML_DTD;

use Data::Dumper;


my $fname = shift || die "usage: $0 <file name>\n";

open (my $F, $fname) or die "cannot open input file: $!\n";
binmode $F;
undef $/; # slurp the input file

my $wbxml = <$F>;

close $F;

my $doc = new BinaryDocument;
$doc->set_string($wbxml);

#print Dumper($WBXML_DTD::SyncML_DTD);

my $parser = new WBXML_Parser;
#TODO parse_header(), get public id, set DTD accordingly
$parser->set_dtd($WBXML_DTD::SyncML_DTD);

my $xml = $parser->parse($doc);

#print Dumper($xml);
$xml->printxml(*STDOUT);

#$xml->find_elem_by_name('CmdID', sub {my $elem = shift; print "Cmd $elem->{children}[0]\n"});
#my $alert;
#$xml->find_elem_by_name('Alert', sub {$alert = shift;});
#if ($alert) {
#    $xml->find_elem_by_name('Data', sub {my $elem = shift; print "Alert code $elem->{children}[0]\n"}, $alert);
#}

# find the data of Put, decode&printxml
#my $put;
#$xml->find_elem_by_name('Put', sub {$put = shift;});
#if ($put) {
#    my $opaque;
#    $xml->find_elem_by_name('Data', sub {my $d = shift; $opaque = $d->{children}[0]}, $put);
#    if ($opaque && $opaque->{name} eq 'WBXML OPAQUE') {
#        my $opdoc = new BinaryDocument;
#        $opdoc->set_string($opaque->{data});
#
#        #TODO this is not SyncML, we need DevInf DTD!
#        my $opxml = $parser->parse($opdoc);
#        $opxml->printxml(*STDOUT);
#    } else {
#        die "can't find the opaque\n"
#    }
#}

if (0) {
    my $doc2 = $parser->encode($xml);
    open (my $F2, ">", "$fname de-encoded.txt") or die "cannot open output file: $!\n";
    binmode $F2;
    print $F2 $doc2->get_string;
    close $F2;
}

#my $skel = new WBXML;
#$skel->create_syncml_skeleton;
#$skel->printxml(*STDOUT);
