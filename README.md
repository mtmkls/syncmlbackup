# SyncML Backup

A simple tool to backup phone calendar/contacts/notes using SyncML protocol.

It is not a calendar server. It is not a groupware suite. It is not even a conformant SyncML implementation. It is just the bare minimum needed to convince a SyncML client to upload its data.

I created this to extract the calendar entries from my old Sony Ericsson phone. Not tested with anything else.

# Usage

You need a http server running on a machine your phone can connect to. I used Lighttpd.

Put all of these files into the directory served by the web server, and enable CGI with Perl support.

Create a working directory where the results will be saved. It must be writable by the `www-root` user or group. Edit the `$work_dir` variable in index.pl to point at your working directory.

You can test the install with Firefox or Curl. The index.pl should return a nice welcome message for GET and POST requests.

Optional: enable SSL/TLS.

Set up the phone to use `your.server/directory/index.pl` as the synchronization server. Database names and authentication info are ignored by the server.

Start synchronization. The results written to the work directory are

- a detailed debug log
- every WBXML packet received and sent (for offline analysis if something went wrong, parsesyncml.pl should be able to decode the binary format)
- each VCard sent by the phone is saved to a separate file

Have Fun!

# MIT License

Copyright (c) 2019 Miklós Máté

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

