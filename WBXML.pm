package WBXML;

use strict;
use warnings;

use WBXML_Elem;

sub new {
    my $class = shift;
    return bless {}, $class
}

sub traverse {
    my $self = shift;
    my $elem = shift;
    my $callbacks = shift;

    # all callbacks are optional
    if (defined $callbacks->{elemobj}) {
        $callbacks->{elemobj}->($elem)
    }
    if (defined $callbacks->{elemname}) {
        $callbacks->{elemname}->($elem->{name},
            defined $elem->{attrs}, defined $elem->{children})
    }

    if (defined $elem->{attrs}) {
        if (defined $callbacks->{attr}) {
            $callbacks->{attr}->($_->{name}, $_->{value}) for @{$elem->{attrs}}
        }
    }
    if (defined $callbacks->{elemname_done}) {
        $callbacks->{elemname_done}->($elem->{name},
            defined $elem->{attrs}, defined $elem->{children})
    }

    if (defined $elem->{children}) {
        for my $child (@{$elem->{children}}) {
            if (ref $child eq '') {
                # not a reference, most likely a string value
                if (defined $callbacks->{string}) {
                    $callbacks->{string}->($child)
                }
            } elsif (ref $child eq 'HASH') {
                # currently we only do this for OPAQUE
                if (defined $callbacks->{opaque}) {
                    $callbacks->{opaque}->($child->{data})
                }
            } elsif ($child->isa('WBXML_Elem')) {
                $self->traverse($child, $callbacks)
            } else {
                die "unexpected reference in WBXML tree\n";
            }
        }
    }

    if (defined $callbacks->{elem_done}) {
        $callbacks->{elem_done}->($elem->{name},
            defined $elem->{attrs}, defined $elem->{children})
    }
}

sub printxml {
    my $self = shift;
    my $F = shift;

    #TODO stringtable may contain multiple null-terminated strings
    #   (the phone never does that, and doesn't null-terminate the only string, which is the pubid)
    print $F "<WBXML version=\"$self->{version}\" pubid=\"$self->{pubid}\" charset=$self->{charset} stringtable=\"$self->{stringtable}\">\n";

    #TODO check that root->isa('WBXML_Elem')
    my $depth = 0;
    $self->traverse($self->{root}, {
            elemname => sub {
                my ($name, $hasattr, $haschild) = @_;
                print $F '  ' x $depth;
                print $F "<$name";
                #TODO if ($hasattr) {
                #} else {
                #}
            },
            attr => sub {
                my ($name, $value) = @_;
                print $F " $name=\"$value\""
            },
            elemname_done => sub {
                my ($name, $hasattr, $haschild) = @_;
                if ($haschild) {
                    print $F ">\n";
                    $depth++;
                } else {
                    print $F "/>\n"
                }
            },
            elem_done => sub {
                my ($name, $hasattr, $haschild) = @_;
                if ($haschild) {
                    $depth--;
                    print $F '  ' x $depth;
                    print $F "</$name>\n";
                }
            },
            string => sub {
                my $str = shift;
                print $F '  ' x $depth;
                print $F "$str\n";
            },
            opaque => sub {
                my $opaque = shift;
                print $F '  ' x $depth;
                print $F "[OPAQUE data ", length $opaque, " bytes]\n";
                print $F "$opaque\n"
            },
        })
    #TODO check that $depth=0
}

sub get_pubid_string {
    my $self = shift;

    my $offs = $self->{pubid};
    my $end = index($self->{stringtable}, '\0', $offs);
    if ($end > 0) {
        return substr($self->{stringtable}, $offs, $end-$offs)
    } else {
        return substr($self->{stringtable}, $offs)
    }
}

sub find_elem_by_name {
    my $self = shift;
    my $searchname = shift;
    my $callback = shift;
    my $root = shift || $self->{root};

    $self->traverse($root, {
            elemobj => sub {
                my $elem = shift;

                if ($elem->{name} eq $searchname) {
                    $callback->($elem)
                }
            }
        });
}

# create node that represents OPAQUE data of WBXML
sub create_opaque {
    my @data = @_;

    return {name=>'WBXML OPAQUE', data=>pack "C*", @data}
}

sub create_syncml_skeleton {
    my $self = shift;

    $self->{version} = '1.2';
    $self->{pubid} = 0;
    $self->{charset} = 0x6a;
    $self->{stringtable} = '-//SYNCML//DTD SyncML 1.2//EN';

    $self->{root} = new WBXML_Elem('SyncML');

    my $hdr = new WBXML_Elem('SyncHdr');
    $self->{root}->add_child($hdr);

    my $verdtd = new WBXML_Elem('VerDTD');
    $verdtd->add_child('1.2');
    $hdr->add_child($verdtd);

    my $verproto = new WBXML_Elem('VerProto');
    $verproto->add_child('SyncML/1.2');
    $hdr->add_child($verproto);

    my $body = new WBXML_Elem('SyncBody');
    $self->{root}->add_child($body);
}

1;

=encoding utf8

=head1 WBXML tree representation

=head1 Author Miklós Máté E<lt>mtmkls@gmail.comE<gt>

=head1 License MIT

=cut
